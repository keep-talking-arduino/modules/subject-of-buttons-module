#include <Arduino.h>
#include <CommandHandler.h>
#include <ShiftHandler.h>
#include <LiquidCrystal_I2C.h>
#include <bigChar.h>
#include <Button.h>


CommandHandler cmdHandler;
ShiftHandler shiftHandler;
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
Button theButton;

// set default module variables
bool module_Active = false;
int module_statusBarValue = 0;

// list of text and color possibilities
String LIST_COLORS[] = {"BLUE", "WHITE", "YELLOW", "GREEN", "RED", "PURPLE"};
String LIST_TEXTS[] = {"ABORT", "DETONATE", "HOLD", "PRESS", "STOP", "PUSH"};

// set LED output pins
int btn_redPin = A0;
int btn_greenPin = A1;
int btn_bluePin = A2;
int strip_redPin = 5;
int strip_greenPin = 6;
int strip_bluePin = 9;

bool refreshDispay = false;

// initialize game values
bool shouldHoldButton;
int module_batteries = -1;
bool module_FRK = false;
bool module_CAR = false;
String releaseAt = "x";
String strip_color = "x";
String btn_color = "x";
String btn_text = "";
String clock_time = "x";

void generateButton(int batteries, bool CAR_lit, bool FRK_lit){

  // give button random color
  int randBtnColor = random(6);
  btn_color = LIST_COLORS[randBtnColor];
  //give button random text
  int randText = random(6);
  btn_text = LIST_TEXTS[randText];
  // give strip random color
  int randStripColor = random(6);
  strip_color = LIST_COLORS[randStripColor];

  // game logic
  if(btn_color.equals("BLUE") && btn_text.equals("ABORT")){ // 1
    shouldHoldButton = true;
  }else if(batteries > 1 && btn_text.equals("DETONATE")){ // 2
    shouldHoldButton = false;
  }else if(btn_color.equals("WHITE") && CAR_lit){ // 3
    shouldHoldButton = true;
  }else if(batteries > 2 && FRK_lit){ // 4
    shouldHoldButton = false;
  }else if(btn_color.equals("YELLOW")){ // 5
    shouldHoldButton = true;
  }else if(btn_color.equals("RED") && btn_text.equals("HOLD")){ // 6
    shouldHoldButton = false;
  }else{
    shouldHoldButton = true;
  }

  // define at what number the button should be released
  if(shouldHoldButton){
    if(strip_color.equals("BLUE")){
      releaseAt = "4";
    }else if(strip_color.equals("WHITE")){
      releaseAt = "1";
    }else if(strip_color.equals("YELLOW")){
      releaseAt = "5";
    }else{
      releaseAt = "1";
    }
  }
}

void lightColor(int redPin, int greenPin, int bluePin, String color){
  if(color.equals("BLUE")){
    analogWrite(bluePin, 1000);
  }else if(color.equals("RED")){
    analogWrite(redPin, 1000);
    analogWrite(greenPin, 0);
    analogWrite(bluePin, 0);
  }else if(color.equals("WHITE")){
    analogWrite(redPin, 1000);
    analogWrite(greenPin, 500);
    analogWrite(bluePin, 400);
  }else if(color.equals("YELLOW")){
    analogWrite(redPin, 1000);
    analogWrite(greenPin, 600);
    analogWrite(bluePin, 0);
  }else{
    analogWrite(greenPin, 1000);
  }
}

void startModule(){
  generateButton(module_batteries, module_CAR, module_FRK);
  lightColor(btn_redPin, btn_greenPin, btn_bluePin, btn_color);

  refreshDispay = true;
  module_Active = true;
  theButton.setActive(true);
}

void stopModule(){
  // change state to inactive
  module_Active = false;
  theButton.setActive(false);

  // clear display
  btn_text = "";
  refreshDispay = true;

  // reset game values
  module_batteries = -1;
  module_FRK = false;
  module_CAR = false;
  clock_time = "x";

  // clear status bar
  shiftHandler.clear();
  shiftHandler.shift();

  // turn of LEDs
  analogWrite(btn_redPin, 0);
  analogWrite(btn_greenPin, 0);
  analogWrite(btn_bluePin, 0);
  analogWrite(strip_redPin, 0);
  analogWrite(strip_greenPin, 0);
  analogWrite(strip_bluePin, 0);
}

void ProcessCommand(Command cmd){
  Serial.println(cmd.ToString());

  // verify the time returned by MODULE_CLOCK
  if(cmd.GetMessageType().equals(TYPE_RESPONSE) && cmd.GetAction().equals("GETTIME")){
    clock_time = String(cmd.GetValueOnIndex(1)) + String(cmd.GetValueOnIndex(2));
  }else if(cmd.GetAction() == "SETSTATBAR"){
    module_statusBarValue = cmd.GetValue().toInt();
    for(int i = 0; i <= module_statusBarValue; i++){
      shiftHandler.setValue(0, i, true);
    }
    shiftHandler.shift();
  }else if(cmd.GetAction() == "STARTMODULE"){
    startModule();
  }else if(cmd.GetAction() == "STOPMODULE"){
    stopModule();
  }else if(cmd.GetAction() == "SETBATTERIES"){
    module_batteries = cmd.GetValue().toInt();
  }else if(cmd.GetAction() == "SETLABEL"){
    if(cmd.GetValue().equals("FRK")){
      module_FRK = true;
    }else if(cmd.GetValue().equals("CAR")){
      module_CAR = true;
    }
  }
}

void success(){
  Serial.println("Success!");
  cmdHandler.Send(MODULE_CURRENT, MODULE_CTRLUNIT, TYPE_NOTIFY, "DISARMED", "");
  stopModule();
}

void mistake(){
  Serial.println("Mistake!");
  cmdHandler.Send(MODULE_CURRENT, MODULE_CTRLUNIT, TYPE_SET, "ADDSTRIKE", "");
  clock_time = "x";
}

void onPress(Button btn){
  lightColor(strip_redPin, strip_greenPin, strip_bluePin, strip_color);
}

void onRelease(Button btn){
  if(!shouldHoldButton){
    success();
  }else{
    cmdHandler.Send(MODULE_CURRENT, MODULE_CLOCK, TYPE_REQUEST, "GETTIME", "");
    cmdHandler.Run();

    // wait for response from clock
    while(clock_time == "x"){
      delay(5);
    }
    Serial.println(clock_time);

    // check if time contains required digit
    if(clock_time.indexOf(releaseAt) >= 0){
      success();
    }else{
      mistake();
    }
  }
}

void setup(){
  Serial.begin(57600);
  randomSeed(analogRead(3));
  cmdHandler.Begin(MODULE_BUTTON, ProcessCommand);

  // initialize LCD Display
  lcd.begin(16,2);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.noBacklight();

  // initialize and clear shiftHandler
  shiftHandler.init(11, 12, 10);// clock, latch, data
  shiftHandler.clear();
  shiftHandler.shift();

  // initialize outputs
  pinMode(btn_redPin, OUTPUT);
  pinMode(btn_greenPin, OUTPUT);
  pinMode(btn_bluePin, OUTPUT);
  pinMode(strip_redPin, OUTPUT);
  pinMode(strip_greenPin, OUTPUT);
  pinMode(strip_bluePin, OUTPUT);

  // initialize button
  theButton.setInputPin(3);
  theButton.init(onRelease, onPress);

  Serial.println("loaded Button...");
}

void loop(){
  // writes button text to LCD Display (cannot be done inside interrupt)
  if(refreshDispay){
    lcd.clear();
    lcd.setCursor(8 - (btn_text.length() / 2), 0);
    lcd.print(btn_text);
    if(btn_text.length() == 0){
      lcd.noBacklight();
    }else{
      lcd.backlight();
    }

    refreshDispay = false;
  }

  cmdHandler.Run();
  theButton.run();
}
